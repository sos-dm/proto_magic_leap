using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dot : MonoBehaviour
{
    private GameObject canva;
    private GameObject alert;

    void Start() {
        canva = GameObject.Find("Canva");

        alert = new GameObject();
        alert.AddComponent<Transform>();

        Vector3 posAlert = SetupPos.getAlertPos(OnAction.getIdAlert());

        alert.GetComponent<Transform>().position = posAlert;
    }

    void Update() {
        
        GetComponent<Transform>().position = new Vector3(canva.GetComponent<Transform>().position.x, canva.GetComponent<Transform>().position.y, canva.GetComponent<Transform>().position.z);
        GetComponent<Transform>().LookAt(alert.transform);
    }

    void DestroyGameObject()
    {
        Destroy(alert);
    }
}
